import {Component,HostListener,Directive,HostBinding} from '@angular/core';

@Directive({
    selector: '[myTestDirective]'
 })
 
export class MiNuevaDirective {

    @HostBinding('style.backgroundColor') c_colorrr = "red"; 

    @HostListener('mouseenter') c_onEnterrr() {
        this.c_colorrr= "blue" ;
    }

    @HostListener('mouseleave') c_onLeaveee() {
        this.c_colorrr = "yellow" ;
    } 
}