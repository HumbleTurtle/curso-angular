import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.scss']
})
export class ListaDestinosComponent implements OnInit {

  destinos: string[];

  constructor() {

    this.destinos = [
      "Barcelona",
      "Buenos Aires",
      "Lima",
      "Barranquilla"
    ];
    
  }

  ngOnInit(): void {
    
  }

  cliked( destinoInput : any ): void {

    this.destinos.push( destinoInput.value );
    destinoInput.value = ""
  }

}
