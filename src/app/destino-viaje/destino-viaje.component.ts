import { Component, OnInit, Input, Directive } from '@angular/core';
import { MiNuevaDirective } from '../directivas/destino-viaje-click';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.scss'],
  viewProviders : [MiNuevaDirective]
})

export class DestinoViajeComponent implements OnInit {
  @Input() nombre: string;

  constructor() {
    this.nombre = "nombre por defecto";
  }

  ngOnInit(): void {
  }

}
